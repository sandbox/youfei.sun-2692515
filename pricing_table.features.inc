<?php
/**
 * @file
 * pricing_table.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pricing_table_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pricing_table_node_info() {
  $items = array(
    'pricing_table' => array(
      'name' => t('Pricing Table'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
