/**
 * @file pricing-table.js
 */
(function ($) {
  Drupal.behaviors.pricingTableReformat = {
    attach: function (context, settings) {

      $('#appnovation-pricing-table td.pricing-table-header').each(function(){
        $('#appnovation-pricing-table.responsive').show();
        $('#appnovation-pricing-table').show();
        var padding = ($(this).height() - $(this).find('div').height())/2;
        padding = padding >= 25 ? padding : 25;
        $('#appnovation-pricing-table').removeAttr("style");
        $('#appnovation-pricing-table.responsive').removeAttr("style");
        $(this).find('div').css('padding-bottom',padding);
        $(this).find('div').css('padding-top',padding);

      });

      $('#appnovation-pricing-table div.detail').parent().css('padding','15px 0');
      $('#appnovation-pricing-table .col-3.no-heading').css('padding-top',40);
    }
  }
}(jQuery));


