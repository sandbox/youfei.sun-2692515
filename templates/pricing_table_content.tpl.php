<?php
/**
 * @file pricing_table_content.tpl.php
 */
?>
<?php $column_name = preg_replace('@[^a-z0-9-]+@','-', strtolower($host_entity->title)); ?>
<?php switch($field_name):
case 'field_pricing_table_packages': ?>
    <td class='<?php echo $column_name;?>'>
      <div class='package'>
        <?php echo $field_pricing_content ?>
      </div>
    </td>
<?php break; ?>
<?php case 'field_pricing_table_globals': ?>
    <td class='global' colspan='<?php echo $node_count;?>'>
      <div class='global'>
        <?php echo $field_pricing_content ?>
      </div>
    </td></tr>
<?php break; ?>
<?php case 'field_pricing_table_features': ?>
    <td class='<?php echo $column_name;?>'>
    <?php if($field_pricing_feature_status):?>
    <div class='feature-on'></div>
    <?php endif; ?>
    <div class='feature-text'>
    <?php echo $field_pricing_content ?>
    </div>
    </td>
<?php break; ?>
<?php case 'field_pricing_table_metrics': ?>
    <td class='<?php echo $column_name;?>'>
    <?php echo $field_pricing_content ?>
    </td>
<?php break; ?>
<?php case 'field_pricing_table_details': ?>
    <td class='<?php echo $column_name;?>'>
    <div class='detail'>
    <?php echo $field_pricing_content ?>
    </div>
    </td>
<?php break; ?>
<?php endswitch; ?>

