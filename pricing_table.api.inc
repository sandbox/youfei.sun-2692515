<?php
/**
 * @file
 * pricing_table.api.inc
 */

function pricing_table_get_all($type = NULL){
  $query = db_select('pricing_table_header', 'h');
  $result = $query->fields('h');
  if(isset($type)){
    $result = $result->condition('type',$type);
  }
  $result = $result->orderBy('weight')
    ->execute()
    ->fetchAll();
  return $result;
}

function pricing_table_get_entry_by_id($id){
  $query = db_select('pricing_table_header', 'h');
  $result = $query->fields('h')
    ->condition('id',$id)
    ->execute()
    ->fetch();
  return $result;
}

function pricing_table_delete_entry($id){
  db_delete('pricing_table_header')->condition('id',$id)->execute();
}

function pricing_table_write_entry($id,$machine_name,$header,$type,$enabled,$weight){
  $record = array(
    'machine_name' => $machine_name,
    'header' => $header,
    'type' => $type,
    'enabled' => $enabled,
    'weight' => $weight,
  );
  if($id != NULL){
    db_merge('pricing_table_header')->key(array('id' => $id))->fields($record)->execute();
  }else{
    db_merge('pricing_table_header')->key(array('machine_name' => $link_name))->fields($record)->execute();
  }
}

function pricing_table_type_options(){
  return array('0' => 'package',
               '1' => 'global',
               '2' => 'detail',
               '3' => 'feature',
               '4' => 'metric');
}

function pricing_table_delete_all(){
  db_truncate('pricing_table_header')->execute();
}
